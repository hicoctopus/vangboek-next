import * as useAuth0Hooks from "@auth0/auth0-react";
import { Auth0ContextInterface } from "@auth0/auth0-react";
import { PageWithLayout } from "../../../src/components/layouts";
import { render } from "@testing-library/react";
import withAuthRequired from "../../../src/hoc/withAuthRequired";

jest.mock("@auth0/auth0-react", () => {
  return {
    __esModule: true,
    useAuth0: jest.fn(),
  };
});

const mockUseAuthImplementation = (
  isLoading = false,
  isAuthenticated = false,
  login = jest.fn()
): void => {
  ((useAuth0Hooks.useAuth0 as unknown) as jest.Mock<
    Auth0ContextInterface
  >).mockImplementationOnce(() => ({
    isLoading,
    isAuthenticated,
    loginWithRedirect: login,
    user: null,
    logout: jest.fn(),
    getAccessTokenSilently: jest.fn(),
    getAccessTokenWithPopup: jest.fn(),
    getIdTokenClaims: jest.fn(),
    loginWithPopup: jest.fn(),
  }));
};

const createPageComponent = (
  withGetInitialProps = false,
  withGetLayout = false
): PageWithLayout => {
  const ThePage: PageWithLayout = () => {
    return <div>Real page contents</div>;
  };
  if (withGetInitialProps) {
    ThePage.getInitialProps = (context) => context;
  }
  if (withGetLayout) {
    ThePage.getLayout = (page) => page;
  }
  return ThePage;
};

describe("the withAuthRequired higher order component", () => {
  it("should display the real page when the user is authenticated", () => {
    mockUseAuthImplementation(false, true);
    const ThePage = createPageComponent();
    const TestComp = withAuthRequired(ThePage);
    const { queryByText } = render(<TestComp />);
    expect(ThePage.getLayout).toBeUndefined();
    expect(ThePage.getInitialProps).toBeUndefined();
    expect(queryByText(/Real page contents/)).toBeInTheDocument();
  });

  it("should display the real page and keep the layout when the user is authenticated", () => {
    mockUseAuthImplementation(false, true);
    const ThePageWithLayout: PageWithLayout = createPageComponent(false, true);
    const TestComp = withAuthRequired(ThePageWithLayout);
    const { queryByText } = render(<TestComp />);
    expect(TestComp.getLayout).toBeDefined();
    expect(TestComp.getInitialProps).toBeUndefined();
    expect(queryByText(/Real page contents/)).toBeInTheDocument();
  });

  it("should display the real page and keep getInitialProps when the user is authenticated", () => {
    mockUseAuthImplementation(false, true);
    const ThePageWithGetInitialProps = createPageComponent(true);
    const TestComp = withAuthRequired(ThePageWithGetInitialProps);
    const { queryByText } = render(<TestComp />);
    expect(TestComp.getLayout).toBeUndefined();
    expect(TestComp.getInitialProps).toBeDefined();
    expect(queryByText(/Real page contents/)).toBeInTheDocument();
  });

  it("should redirect to login when not loading or authenticated", () => {
    const loginMock = jest.fn();
    mockUseAuthImplementation(false, false, loginMock);
    const TestComp = withAuthRequired(createPageComponent());
    render(<TestComp />);
    expect(loginMock).toHaveBeenCalledWith({
      appState: { returnTo: "/" },
    });
  });

  it("should render the redirecting component when loading and redirecting handler is set", () => {
    const loginMock = jest.fn();
    mockUseAuthImplementation(true, false, loginMock);
    const TestComp = withAuthRequired(createPageComponent());
    const { queryByText } = render(<TestComp />);
    expect(loginMock).not.toHaveBeenCalled();
    expect(queryByText(/Checking your login/)).toBeInTheDocument();
  });
});
