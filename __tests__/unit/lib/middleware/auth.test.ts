/**
 * @jest-environment node
 */

import { verify } from "jsonwebtoken";
import { checkAuthMiddleware } from "../../../../src/lib/middleware/auth";
import { NextApiRequest, NextApiResponse } from "next";
import {
  HttpBadRequest,
  HttpForbidden,
  HttpUnauthorized,
} from "../../../../src/lib/ApiError";

jest.mock("jsonwebtoken", () => {
  return {
    verify: jest.fn((token, getKey, options, cb) => {
      if (token === "test-token") {
        cb(null, { permissions: [] });
      } else if (token === "token-with-permissions") {
        cb(null, { permissions: ["some:permission", "an:other"] });
      } else {
        cb(new Error("wrong token"));
      }
    }),
  };
});

describe("auth middleware", () => {
  const requestHandler = jest.fn();
  const middleware = checkAuthMiddleware(["some:permission"], requestHandler);
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should fail when there is no token", async () => {
    const req: Partial<NextApiRequest> = {};
    const res: Partial<NextApiResponse> = {};
    await expect(
      middleware(<NextApiRequest>req, <NextApiResponse>res)
    ).rejects.toThrow(HttpBadRequest);
    expect(requestHandler).not.toHaveBeenCalled();
  });
  it("should fail on invalid token", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        authorization: "Bearer wrong-token",
      },
    };
    const res: Partial<NextApiResponse> = {};
    await expect(
      middleware(<NextApiRequest>req, <NextApiResponse>res)
    ).rejects.toThrow(HttpUnauthorized);
    expect(verify).toHaveBeenCalledTimes(1);
    expect(requestHandler).not.toHaveBeenCalled();
  });
  it("should check permissions on valid token and fail if there are none", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        authorization: "Bearer test-token",
      },
    };
    const res: Partial<NextApiResponse> = {};
    await expect(
      middleware(<NextApiRequest>req, <NextApiResponse>res)
    ).rejects.toThrow(HttpForbidden);
    expect(verify).toHaveBeenCalledTimes(1);
    expect(requestHandler).not.toHaveBeenCalled();
  });

  it("should check permissions on valid token and call the request handler on success", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        authorization: "Bearer token-with-permissions",
      },
    };
    const res: Partial<NextApiResponse> = {};
    expect(
      await middleware(<NextApiRequest>req, <NextApiResponse>res)
    ).toBeUndefined();
    expect(verify).toHaveBeenCalledTimes(1);
    expect(requestHandler).toHaveBeenCalledWith(
      { ...req, token: { permissions: ["some:permission", "an:other"] } },
      {}
    );
  });
});
