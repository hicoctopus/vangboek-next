/**
 * @jest-environment node
 */

import { baseMiddleware } from "../../../../src/lib/middleware/base";
import { NextApiRequest, NextApiResponse } from "next";

const resMock = (): Partial<NextApiResponse> => {
  const res: Partial<NextApiResponse> = {};
  res.setHeader = jest.fn().mockReturnValue(res);
  res.end = jest.fn().mockReturnValue(res);
  return res;
};

describe("baseMiddleware", () => {
  const requestHandler = jest.fn();
  const middlewareAnyMethod = baseMiddleware("*", requestHandler);
  const middlewarePostAndGetMethod = baseMiddleware(
    ["POST", "GET"],
    requestHandler
  );
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should fail without accept header", async () => {
    const req: Partial<NextApiRequest> = {};
    const res = resMock();

    await middlewareAnyMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).not.toHaveBeenCalled();
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Not Acceptable"}'
    );
  });
  it("should fail with an accept header other than application/json", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "text/html",
      },
    };
    const res = resMock();

    await middlewareAnyMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).not.toHaveBeenCalled();
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Not Acceptable"}'
    );
  });
  it("should fail when the request method is incorrect", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
      },
      method: "OPTIONS",
    };
    const res = resMock();

    await middlewarePostAndGetMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).not.toHaveBeenCalled();
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Method not allowed"}'
    );
  });
  it("should fail when on a POST request without content type but with a body", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
      },
      method: "POST",
      body: "hi",
    };
    const res = resMock();

    await middlewarePostAndGetMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).not.toHaveBeenCalled();
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Unsupported Media Type"}'
    );
  });
  it("should fail when on a POST request with invalid content type and with a body", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
        "content-type": "text/html",
      },
      method: "POST",
      body: "hi",
    };
    const res = resMock();

    await middlewarePostAndGetMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).not.toHaveBeenCalled();
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Unsupported Media Type"}'
    );
  });
  it("should succeed when on a POST request with valid content type and with a body", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
      method: "POST",
      body: "hi",
    };
    const res = resMock();

    await middlewarePostAndGetMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).toHaveBeenCalledWith(req, res);
    expect(res.end).toHaveBeenCalledWith();
  });
  it("should succeed when on a GET request", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
      },
      method: "GET",
    };
    const res = resMock();

    await middlewarePostAndGetMethod(<NextApiRequest>req, <NextApiResponse>res);
    expect(requestHandler).toHaveBeenCalledWith(req, res);
    expect(res.end).toHaveBeenCalledWith();
  });
  it("should return a 500 if the requestHandler throws", async () => {
    const req: Partial<NextApiRequest> = {
      headers: {
        accept: "application/json",
      },
      method: "GET",
    };
    const res = resMock();
    const requestHandlerThatThrows = jest.fn(() => {
      throw new Error("tada!");
    });

    await baseMiddleware("GET", requestHandlerThatThrows)(
      <NextApiRequest>req,
      <NextApiResponse>res
    );
    expect(requestHandlerThatThrows).toHaveBeenCalledWith(req, res);
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Internal server error"}'
    );
  });
});
