import {
  roleHasPermission,
  roleHasPermissions,
  rolesHavePermissions,
} from "../../../../src/lib/auth/permissionCheck";

jest.mock("../../../../src/lib/auth/rolePermissionsMapping", () => {
  return {
    __esModule: true,
    default: {
      visitor: {
        static: [
          "read:page",
          "read:dashboard",
          "read:comment",
          "create:comment",
        ],
      },
      editor: {
        static: [
          "read:page",
          "edit:page",
          "create:page",
          "delete:page",
          "read:dashboard",
          "read:comment",
          "edit:comment",
          "create:comment",
          "delete:comment",
        ],
      },
      admin: {
        static: [
          "read:page",
          "edit:page",
          "create:page",
          "delete:page",
          "read:dashboard",
          "edit:dashboard",
          "read:comment",
          "edit:comment",
          "create:comment",
          "delete:comment",
        ],
      },
      superAdmin: {
        static: [
          "read:dashboard",
          "edit:dashboard",
          "create:dashboard",
          "delete:dashboard",
        ],
      },
    },
  };
});

describe("roleHasPermission", () => {
  it("returns false if the role is unknown", () => {
    expect(roleHasPermission("doesNotExists", "read:page")).toBe(false);
  });
  it("returns false when the role exists without the requested permission", () => {
    expect(roleHasPermission("visitor", "edit:page")).toBe(false);
  });
  it("returns true when the role exists and permission exist", () => {
    expect(roleHasPermission("visitor", "read:page")).toBe(true);
  });
});

describe("roleHasPermissions", () => {
  it("works on single permissions", () => {
    expect(roleHasPermissions("visitor", ["edit:page"])).toBe(false);
    expect(roleHasPermissions("visitor", ["read:page"])).toBe(true);
  });
  it("works on multiple permissions", () => {
    expect(
      roleHasPermissions("visitor", [
        "read:page",
        "edit:page",
        "read:dashboard",
        "read:comment",
        "create:comment",
      ])
    ).toBe(false);
    expect(
      roleHasPermissions("visitor", [
        "read:page",
        "read:dashboard",
        "read:comment",
        "create:comment",
      ])
    ).toBe(true);
  });
});

describe("rolesHavePermissions", () => {
  it("works on single roles and permissions", () => {
    expect(rolesHavePermissions(["visitor"], ["edit:page"])).toBe(false);
    expect(rolesHavePermissions(["visitor"], ["read:page"])).toBe(true);
  });
  it("works on single roles and multiple permissions", () => {
    expect(
      rolesHavePermissions(
        ["visitor"],
        [
          "read:page",
          "edit:page",
          "read:dashboard",
          "read:comment",
          "create:comment",
        ]
      )
    ).toBe(false);
    expect(
      rolesHavePermissions(
        ["visitor"],
        ["read:page", "read:dashboard", "read:comment", "create:comment"]
      )
    ).toBe(true);
  });
  it("works on multiple roles and single permissions", () => {
    expect(
      rolesHavePermissions(["visitor", "editor"], ["edit:dashboard"])
    ).toBe(false);
    expect(rolesHavePermissions(["visitor", "editor"], ["read:comment"])).toBe(
      true
    );
  });
  it("works on multiple roles and permissions", () => {
    expect(
      rolesHavePermissions(
        ["visitor", "superAdmin"],
        ["edit:page", "create:page", "delete:page"]
      )
    ).toBe(false);
    expect(
      rolesHavePermissions(
        ["visitor", "superAdmin"],
        ["read:page", "edit:dashboard"]
      )
    ).toBe(true);
  });
});
