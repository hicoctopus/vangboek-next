/**
 * @jest-environment node
 */

import pino from "pino";
import { getLogger, mainLogger } from "../../../src/lib/logging";

jest.mock("pino", () => {
  return jest.fn(() => {
    return { child: jest.fn() };
  });
});

describe("the mainLogger", () => {
  it("should be created", () => {
    expect(pino).toHaveBeenCalledWith({
      prettyPrint: false,
      level: "debug",
      enabled: false, // as this is the test environment
    });
  });
});

describe("getLogger", () => {
  it("should create a new logger", () => {
    getLogger("test");
    expect(mainLogger.child).toHaveBeenCalledWith({
      module: "test",
    });
  });
});
