/**
 * @jest-environment node
 */

import { ApiError, ValidationError } from "../../../src/lib/ApiError";
import { NextApiResponse } from "next";

// example of unit test
// NOTE THE CHANGE OF JEST ENVIRONMENT AT THE BEGINNING OF THE FILE

const resMock = (): Partial<NextApiResponse> => {
  const res: Partial<NextApiResponse> = {};
  res.setHeader = jest.fn().mockReturnValue(res);
  res.end = jest.fn().mockReturnValue(res);
  return res;
};

describe("an ApiError", () => {
  it("can be constructed with one argument", () => {
    const apiError = new ApiError("test-message");
    expect<ApiError>(apiError).toBeInstanceOf(Error);
    expect(apiError.getMessage()).toEqual("test-message");
    expect(apiError.getStatusCode()).toEqual(500);
  });

  describe("has getters", () => {
    const error = new ApiError("test-message", 999);
    it("that return statusCode", () => {
      expect(error.getStatusCode()).toEqual(999);
    });
    it("that return message", () => {
      expect(error.getMessage()).toEqual("test-message");
    });
  });

  describe("has setters", () => {
    const error = new ApiError("test-message", 999);
    it("that set statusCode", () => {
      error.setStatusCode(200);
      expect(error.getStatusCode()).toEqual(200);
    });
    it("that set message", () => {
      error.setMessage("another");
      expect(error.getMessage()).toEqual("another");
    });
  });

  it("can be converted to string", () => {
    const error = new ApiError("test-message");
    expect(error.toString()).toEqual("test-message - StatusCode: 500.");
  });
  it("can return a pretty message", () => {
    const error = new ApiError("test-message");
    expect(error.getPrettyMessage()).toEqual("test-message - StatusCode: 500.");
  });
  it("returns a json response", () => {
    const error = new ApiError("test");
    const res = resMock();
    error.sendToClient(res as NextApiResponse);
    expect(res.setHeader).toHaveBeenCalledWith(
      "content-type",
      "application/json"
    );
    expect(res.end).toHaveBeenCalledWith('{"error":true,"message":"test"}');
    expect(res.statusCode).toEqual(500);
  });
});

describe("an ValidationError", () => {
  it("could exist without validated fields", () => {
    const res = resMock();
    const error = new ValidationError();
    error.sendToClient(res as NextApiResponse);
    expect(res.setHeader).toHaveBeenCalledWith(
      "content-type",
      "application/json"
    );
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Validation error","validation":{}}'
    );
    expect(res.statusCode).toEqual(400);
  });
  it("with violated fields through the constructor", () => {
    const res = resMock();
    const error = new ValidationError("field1", "This field is required");
    error.sendToClient(res as NextApiResponse);
    expect(res.setHeader).toHaveBeenCalledWith(
      "content-type",
      "application/json"
    );
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Validation error","validation":{"field1":"This field is required"}}'
    );
    expect(res.statusCode).toEqual(400);
  });
  it("with violated fields through the constructor and the method", () => {
    const res = resMock();
    const error = new ValidationError("field1", "This field is required");
    error.addViolatedField("fied2", "This field should not be empty");
    error.sendToClient(res as NextApiResponse);
    expect(res.setHeader).toHaveBeenCalledWith(
      "content-type",
      "application/json"
    );
    expect(res.end).toHaveBeenCalledWith(
      '{"error":true,"message":"Validation error","validation":{"field1":"This field is required","fied2":"This field should not be empty"}}'
    );
    expect(res.statusCode).toEqual(400);
  });
});
