import * as auth0 from "@auth0/auth0-react";
import { render } from "@testing-library/react";
import { UserCan } from "../../../src/components/UserCan";
import { JWTKeys } from "../../../src/lib/auth";
import { Auth0ContextInterface } from "@auth0/auth0-react";

jest.mock("@auth0/auth0-react", () => {
  return {
    __esModule: true,
    useAuth0: jest.fn(),
  };
});

jest.mock("../../../src/lib/auth/rolePermissionsMapping", () => {
  return {
    __esModule: true,
    default: {
      visitor: {
        static: [
          "read:page",
          "read:dashboard",
          "read:comment",
          "create:comment",
        ],
      },
      editor: {
        static: [
          "read:page",
          "edit:page",
          "create:page",
          "delete:page",
          "read:dashboard",
          "read:comment",
          "edit:comment",
          "create:comment",
          "delete:comment",
        ],
      },
      admin: {
        static: [
          "read:page",
          "edit:page",
          "create:page",
          "delete:page",
          "read:dashboard",
          "edit:dashboard",
          "read:comment",
          "edit:comment",
          "create:comment",
          "delete:comment",
        ],
      },
      superAdmin: {
        static: [
          "read:dashboard",
          "edit:dashboard",
          "create:dashboard",
          "delete:dashboard",
        ],
      },
    },
  };
});

const mockUseAuthImplementation = (
  user: null | unknown = null,
  isLoading = false,
  isAuthenticated = false,
  login = jest.fn()
): void => {
  ((auth0.useAuth0 as unknown) as jest.Mock<
    Auth0ContextInterface
  >).mockImplementationOnce(() => ({
    isLoading,
    isAuthenticated,
    user: user,
    logout: jest.fn(),
    loginWithRedirect: login,
    loginWithPopup: jest.fn(),
    getIdTokenClaims: jest.fn(),
    getAccessTokenWithPopup: jest.fn(),
    getAccessTokenSilently: jest.fn(),
  }));
};

describe("component UserCan", () => {
  it("chooses the right component to render - positive", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"read:page"}
        yes={() => <div>Yes, the user can.</div>}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeInTheDocument();
    expect(queryByText(/No, the user can not./)).toBeNull();
  });
  it("chooses the right component to render - negative", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"edit:page"}
        yes={() => <div>Yes, the user can.</div>}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeNull();
    expect(queryByText(/No, the user can not./)).toBeInTheDocument();
  });
  it("allows only one case defined - yes positive", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"read:page"}
        yes={() => <div>Yes, the user can.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeInTheDocument();
  });
  it("allows only one case defined - yes negative", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"edit:page"}
        yes={() => <div>Yes, the user can.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeNull();
  });
  it("allows only one case defined - no positive", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"read:page"}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/No, the user can not./)).toBeNull();
  });
  it("allows only one case defined - no negative", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={"edit:page"}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/No, the user can not./)).toBeInTheDocument();
  });
  it("allows multiple permissions - positive", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["editor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={["read:page", "edit:page"]}
        yes={() => <div>Yes, the user can.</div>}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeInTheDocument();
    expect(queryByText(/No, the user can not./)).toBeNull();
  });
  it("allows multiple permissions - negative", () => {
    mockUseAuthImplementation(
      {
        [JWTKeys.ROLES]: ["visitor"],
      },
      false,
      true
    );
    const { queryByText } = render(
      <UserCan
        permission={["read:page", "edit:page"]}
        yes={() => <div>Yes, the user can.</div>}
        no={() => <div>No, the user can not.</div>}
      />
    );
    expect(queryByText(/Yes, the user can./)).toBeNull();
    expect(queryByText(/No, the user can not./)).toBeInTheDocument();
  });
});
