import { render } from "@testing-library/react";
import { ContentPaper } from "../../../../src/components/content/ContentPaper";

// this is an example how components can be tested

describe("correct ContentPaper component", () => {
  it("should render its children", () => {
    const { queryByText } = render(<ContentPaper>The child</ContentPaper>);
    const child = queryByText(/The child/);
    expect(child).toBeInTheDocument();
  });
  it("should accept custom class names", () => {
    const { queryByText } = render(
      <ContentPaper className={"test-classname"}>The child</ContentPaper>
    );
    const child = queryByText(/The child/);
    expect(child).toHaveClass("test-classname");
  });
});
