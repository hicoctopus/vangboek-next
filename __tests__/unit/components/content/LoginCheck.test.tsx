import { render } from "@testing-library/react";
import { LoginCheck } from "../../../../src/components/content/LoginCheck";

describe("correct LoginCheck component", () => {
  it("should render informative text", () => {
    const { queryByText } = render(<LoginCheck />);
    expect(queryByText(/Checking your login/)).toBeInTheDocument();
    expect(
      queryByText(/Please wait while we check if you are authenticated/)
    ).toBeInTheDocument();
  });
  it("should be unchanged", () => {
    const { container } = render(<LoginCheck />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
