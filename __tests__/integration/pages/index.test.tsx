import { render } from "@testing-library/react";
import Index from "../../../src/pages";

// This is an example test that shows how full pages can be tested

describe("the home page", () => {
  it("renders the theme picker", () => {
    const { getByText } = render(<Index />);
    const linkElement = getByText(/System theme/);
    expect(linkElement).toBeInTheDocument();
  });
  it("renders unchanged", () => {
    const { container } = render(<Index />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
