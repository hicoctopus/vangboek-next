module.exports = {
  collectCoverageFrom: [
    "src/**/*.{js,jsx,ts,tsx}",
    "!**/*.d.ts",
    "!**/node_modules/**",
  ],
  coverageReporters: ["text", "html", "cobertura"],
  setupFilesAfterEnv: ["<rootDir>/__tests__/setupTests.js"],
  testPathIgnorePatterns: [
    "/node_modules/",
    "/.next/",
    "<rootDir>/__tests__/setupTests.js",
    "<rootDir>/__tests__/cssTransform.js",
  ],
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
    "^.+\\.css$": "<rootDir>/__tests__/cssTransform.js",
  },
  transformIgnorePatterns: [
    "/node_modules/",
    "^.+\\.module\\.(css|sass|scss)$",
  ],
  moduleNameMapper: {
    "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy",
  },
};
