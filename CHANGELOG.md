# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0-0 (2020-06-02)

### Features

- **api:** implement middleware ([fc5b763](https://gitlab.com/hicoctopus/vangboek-next/commit/fc5b7632fc748529db7eaa506a0b6266fe5fdf82))
- **auth:** implement client side login ([3368b3e](https://gitlab.com/hicoctopus/vangboek-next/commit/3368b3ec325e2dd35f53a5dcd4e4c8dae589828c))
- **auth:** require logged in hoc component for pages ([2f4873c](https://gitlab.com/hicoctopus/vangboek-next/commit/2f4873c35ecaca5524e3bfc43c46c42de9d8088d))
- **auth:** use refreshtoken rotation ([e119953](https://gitlab.com/hicoctopus/vangboek-next/commit/e11995315508835f24f9c8c388008d7a02c8cc7f))
- **design:** add dashboardlayout ([9eaee62](https://gitlab.com/hicoctopus/vangboek-next/commit/9eaee62e0787e08b9e187b11252c269e10bb0628))
- **design:** first steps in responsive menue ([109fa4b](https://gitlab.com/hicoctopus/vangboek-next/commit/109fa4bf50a8cc43f424a89c7c9bc615f956d4f1))
- **design:** setup material-ui ([a0f683e](https://gitlab.com/hicoctopus/vangboek-next/commit/a0f683ebcfe71a1cc73c7bdb415f163b197aaaa0))
- **ui:** mobile menu ([58aa177](https://gitlab.com/hicoctopus/vangboek-next/commit/58aa17753ab4db14e9d694b2d26850e81c313eb1))

### Bug Fixes

- **api:** correct auth logic and types, promise in api and cosmetics ([27c7ca5](https://gitlab.com/hicoctopus/vangboek-next/commit/27c7ca5ac59bd124c56641349169eefbb01af770))
- **deps:** update dependency @material-ui/core to v4.10.0 ([c222823](https://gitlab.com/hicoctopus/vangboek-next/commit/c2228234195c387ba3d478c39bf39ffe93284c20))
- **deps:** update dependency pino to v6.3.0 ([40d9da2](https://gitlab.com/hicoctopus/vangboek-next/commit/40d9da24e0b94d1544496f4a84f303c9a606a53a))
- **deps:** update dependency pino to v6.3.1 ([f6e82ed](https://gitlab.com/hicoctopus/vangboek-next/commit/f6e82ed80f06cd7f42bae3506a7e5752dbe95684))

### Reverts

- Revert "chore(release): 1.0.0-0" ([03d43ff](https://gitlab.com/hicoctopus/vangboek-next/commit/03d43ffb1549532dfa4a3c8961724da06a3fa205))

### Styles

- remove zeit logo from public folder ([855e88c](https://gitlab.com/hicoctopus/vangboek-next/commit/855e88c8baf75fb3c60ddc8abba86a51eb8f6061))
