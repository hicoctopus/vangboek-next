--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-05-10 22:34:19 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2968 (class 1262 OID 16384)
-- Name: vangboek; Type: DATABASE; Schema: -; Owner: vangboek
--

-- CREATE DATABASE vangboek WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE vangboek OWNER TO vangboek;

\connect vangboek

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 16385)
-- Name: vangboek; Type: SCHEMA; Schema: -; Owner: vangboek
--

CREATE SCHEMA vangboek;


ALTER SCHEMA vangboek OWNER TO vangboek;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 16386)
-- Name: entry; Type: TABLE; Schema: vangboek; Owner: vangboek
--

CREATE TABLE vangboek.entry (
    id integer NOT NULL,
    title character varying(54) NOT NULL,
    description character varying(121)
);


ALTER TABLE vangboek.entry OWNER TO vangboek;

--
-- TOC entry 204 (class 1259 OID 16389)
-- Name: entry_id_seq; Type: SEQUENCE; Schema: vangboek; Owner: vangboek
--

ALTER TABLE vangboek.entry ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME vangboek.entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16391)
-- Name: user; Type: TABLE; Schema: vangboek; Owner: vangboek
--

CREATE TABLE vangboek.user (
    id integer NOT NULL,
    armennaam character varying(54) NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE vangboek.user OWNER TO vangboek;

--
-- TOC entry 206 (class 1259 OID 16394)
-- Name: gebruiker_id_seq; Type: SEQUENCE; Schema: vangboek; Owner: vangboek
--

ALTER TABLE vangboek.user ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME vangboek.gebruiker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 207 (class 1259 OID 16396)
-- Name: till; Type: TABLE; Schema: vangboek; Owner: vangboek
--

CREATE TABLE vangboek.till (
    id integer NOT NULL,
    naam character varying(54),
    account_id integer NOT NULL,
    user_id integer
);


ALTER TABLE vangboek.till OWNER TO vangboek;

--
-- TOC entry 208 (class 1259 OID 16399)
-- Name: till_id_seq; Type: SEQUENCE; Schema: vangboek; Owner: vangboek
--

ALTER TABLE vangboek.till ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME vangboek.till_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 209 (class 1259 OID 16401)
-- Name: account; Type: TABLE; Schema: vangboek; Owner: vangboek
--

CREATE TABLE vangboek.account (
    id integer NOT NULL,
    description character varying(54) NOT NULL
);


ALTER TABLE vangboek.account OWNER TO vangboek;

--
-- TOC entry 210 (class 1259 OID 16404)
-- Name: account_id_seq; Type: SEQUENCE; Schema: vangboek; Owner: vangboek
--

ALTER TABLE vangboek.account ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME vangboek.account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 211 (class 1259 OID 16406)
-- Name: transaction; Type: TABLE; Schema: vangboek; Owner: vangboek
--

CREATE TABLE vangboek.transaction (
    id integer NOT NULL,
    comment character varying(54),
    amount numeric(5,2) NOT NULL,
    contra_account_id integer NOT NULL,
    entry_id integer NOT NULL
);


ALTER TABLE vangboek.transaction OWNER TO vangboek;

--
-- TOC entry 212 (class 1259 OID 16409)
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: vangboek; Owner: vangboek
--

ALTER TABLE vangboek.transaction ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME vangboek.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 2818 (class 2606 OID 16412)
-- Name: entry entry_pkey; Type: CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.entry
    ADD CONSTRAINT entry_pkey PRIMARY KEY (id);


--
-- TOC entry 2820 (class 2606 OID 16414)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2824 (class 2606 OID 16416)
-- Name: till till_pkey; Type: CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.till
    ADD CONSTRAINT till_pkey PRIMARY KEY (id);


--
-- TOC entry 2827 (class 2606 OID 16418)
-- Name: account account_pkey; Type: CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- TOC entry 2830 (class 2606 OID 16420)
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- TOC entry 2821 (class 1259 OID 16421)
-- Name: user_account_index; Type: INDEX; Schema: vangboek; Owner: vangboek
--

CREATE INDEX user_account_index ON vangboek.user USING btree (account_id);


--
-- TOC entry 2822 (class 1259 OID 16422)
-- Name: till_user_index; Type: INDEX; Schema: vangboek; Owner: vangboek
--

CREATE INDEX till_user_index ON vangboek.till USING btree (user_id);


--
-- TOC entry 2825 (class 1259 OID 16423)
-- Name: till_account_index; Type: INDEX; Schema: vangboek; Owner: vangboek
--

CREATE INDEX till_account_index ON vangboek.till USING btree (account_id);


--
-- TOC entry 2828 (class 1259 OID 16424)
-- Name: transaction_entry_index; Type: INDEX; Schema: vangboek; Owner: vangboek
--

CREATE INDEX transaction_entry_index ON vangboek.transaction USING btree (entry_id);


--
-- TOC entry 2831 (class 1259 OID 16425)
-- Name: transaction_contra_account_index; Type: INDEX; Schema: vangboek; Owner: vangboek
--

CREATE INDEX transaction_contra_account_index ON vangboek.transaction USING btree (contra_account_id);


--
-- TOC entry 2832 (class 2606 OID 16426)
-- Name: user user_account_id_fkey; Type: FK CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.user
    ADD CONSTRAINT user_account_id_fkey FOREIGN KEY (account_id) REFERENCES vangboek.account(id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 2833 (class 2606 OID 16431)
-- Name: till till_user_id_fkey; Type: FK CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.till
    ADD CONSTRAINT till_user_id_fkey FOREIGN KEY (user_id) REFERENCES vangboek.user(id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 2834 (class 2606 OID 16436)
-- Name: till till_account_id_fkey; Type: FK CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.till
    ADD CONSTRAINT till_account_id_fkey FOREIGN KEY (account_id) REFERENCES vangboek.account(id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 2835 (class 2606 OID 16441)
-- Name: transaction transaction_entry_id_fkey; Type: FK CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.transaction
    ADD CONSTRAINT transaction_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES vangboek.entry(id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 2836 (class 2606 OID 16446)
-- Name: transaction transaction_contra_account_id_fkey; Type: FK CONSTRAINT; Schema: vangboek; Owner: vangboek
--

ALTER TABLE ONLY vangboek.transaction
    ADD CONSTRAINT transaction_contra_account_id_fkey FOREIGN KEY (contra_account_id) REFERENCES vangboek.account(id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


-- Completed on 2020-05-10 22:34:20 CEST

--
-- PostgreSQL database dump complete
--

