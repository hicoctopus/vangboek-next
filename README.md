# vangboek-next

[![pipeline status](https://gitlab.com/hicoctopus/vangboek-next/badges/master/pipeline.svg)](https://gitlab.com/hicoctopus/vangboek-next/-/commits/master) [![coverage report](https://gitlab.com/hicoctopus/vangboek-next/badges/master/coverage.svg)](https://gitlab.com/hicoctopus/vangboek-next/-/commits/master)

Split costs among friends using a shared account.
