import { useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { PageWithLayout } from "../components/layouts";
import { AppState } from "@auth0/auth0-react/dist/auth0-provider";
import { LoginCheck } from "../components/content/LoginCheck";

/**
 * Returns the current page as state
 */
function getReturnTo(): AppState {
  return {
    returnTo: global.window?.location.pathname,
  };
}

/**
 * Makes sure the user is logged in on this page. Redirects to the login if required.
 * @param Page
 */
export default function withAuthRequired(Page: PageWithLayout): PageWithLayout {
  const WrappedPage: PageWithLayout = () => {
    const { isLoading, isAuthenticated, loginWithRedirect } = useAuth0();

    useEffect(() => {
      if (isLoading || isAuthenticated) {
        // if loading or is authenticated, don't do anything.
        return;
      }
      // else
      loginWithRedirect({ appState: getReturnTo() });
    }, [isLoading, isAuthenticated, loginWithRedirect]);

    if (isAuthenticated) {
      return <Page />;
    } else {
      return <LoginCheck />;
    }
  };

  WrappedPage.displayName = Page.displayName;

  // Do the next and layout functions stuff
  if (typeof Page.getInitialProps !== "undefined") {
    WrappedPage.getInitialProps = Page.getInitialProps;
  }
  if (typeof Page.getLayout !== "undefined") {
    WrappedPage.getLayout = Page.getLayout;
  }
  return WrappedPage;
}
