import { createContext } from "react";

export type ThemeStyle = "light" | "dark" | "system";

interface ThemeStyleContextValue {
  themeStyle: ThemeStyle;
  setThemeStyle: (style: ThemeStyle) => void;
}

export const ThemeStyleContext = createContext<ThemeStyleContextValue>({
  themeStyle: "system",
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setThemeStyle: () => {},
});
