import { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

// TODO: needs to be really tought of, this is just an example
interface BaseApiResponse {
  total?: number;
}

type UseApiState<T extends BaseApiResponse = BaseApiResponse> = {
  error: null | Error;
  loading: boolean;
  data: null | T;
  refresh: () => void;
};

/**
 * Call an api route where authentication is required.
 * NOTE: this probably is not needed / will be incorporated with SWR
 * @param path
 * @param scope
 */
export const useApi = (path: string, scope: string): UseApiState => {
  const { getAccessTokenSilently } = useAuth0();
  const [state, setState] = useState({
    error: null,
    loading: true,
    data: null,
  });
  const [refreshIndex, setRefreshIndex] = useState(0);

  useEffect(() => {
    (async () => {
      try {
        const accessToken = await getAccessTokenSilently({
          audience: process.env.NEXT_PUBLIC_API_IDENTIFIER || "",
          scope: scope,
        });
        const res = await fetch(`${window.location.origin}${path}`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            Accept: "application/json",
          },
        });
        setState({
          ...state,
          data: await res.json(),
          error: null,
          loading: false,
        });
      } catch (error) {
        setState({
          ...state,
          error,
          loading: false,
        });
      }
    })();
  }, [refreshIndex]);

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
};
