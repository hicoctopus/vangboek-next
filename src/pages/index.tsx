import { useCallback, useContext } from "react";
import Head from "next/head";
import {
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Typography,
} from "@material-ui/core";
import { ThemeStyleContext } from "../contexts/ThemeStyleContext";
import { getLayout } from "../components/layouts/DashboardLayout";
import { PageWithLayout } from "../components/layouts";
import { useAuth0 } from "@auth0/auth0-react";
import { ContentPaper } from "../components/content/ContentPaper";

const Home: PageWithLayout = () => {
  const { themeStyle, setThemeStyle } = useContext(ThemeStyleContext);
  const onChangeCallback = useCallback(
    (event) => setThemeStyle(event.target.value),
    [setThemeStyle]
  );

  const { isAuthenticated, user } = useAuth0();
  return (
    <div>
      <Head>
        <title>Home</title>
      </Head>
      <ContentPaper>
        <FormControl>
          <InputLabel id="demo-simple-select-label">Theme</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={themeStyle}
            onChange={onChangeCallback}
          >
            <MenuItem value="system">System theme</MenuItem>
            <MenuItem value="light">Light</MenuItem>
            <MenuItem value="dark">Dark</MenuItem>
          </Select>
        </FormControl>
      </ContentPaper>
      {isAuthenticated && (
        <ContentPaper>
          <Typography>You are now logged in. Userdata:</Typography>
          <pre>{JSON.stringify(user, null, 2)}</pre>
        </ContentPaper>
      )}
    </div>
  );
};

Home.getLayout = getLayout;

export default Home;
