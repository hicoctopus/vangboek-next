import { base, checkAuth } from "../../lib/middleware";

const data = base(
  "GET",
  checkAuth(["read:data"], async (req, res) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        res.json({
          title: "super secret",
          body: "this is a secret message from the api.",
          permissions: req.token.permissions,
        });
        resolve();
      }, 500);
    });
  })
);

export default data;
