import { FunctionComponent, useState } from "react";
import Head from "next/head";
import { Typography, Button, Box, CircularProgress } from "@material-ui/core";
import { getLayout } from "../components/layouts/DashboardLayout";
import { PageWithLayout } from "../components/layouts";
import { useAuth0 } from "@auth0/auth0-react";
import withAuthRequired from "../hoc/withAuthRequired";
import { ContentPaper } from "../components/content/ContentPaper";
import { UserCan } from "../components/UserCan";
import { useApi } from "../hooks/useApi";

const ApiResult: FunctionComponent = () => {
  const { loading, error, data } = useApi("/api/data", "read:data");
  return (
    <>
      {loading && (
        <Box>
          <Typography>Loading data ...</Typography>
          <CircularProgress />
        </Box>
      )}
      {error && (
        <Box>
          <Typography>Auth0 error:</Typography>
          <pre>{error ? JSON.stringify(error, null, 2) : "null"}</pre>
        </Box>
      )}
      <Typography>Data:</Typography>
      <pre>{data !== null ? JSON.stringify(data, null, 2) : "null"}</pre>
    </>
  );
};

const Other: PageWithLayout = () => {
  const { isAuthenticated, user } = useAuth0();

  const [callApi, setCallApi] = useState<boolean>(false);

  if (!isAuthenticated) {
    return (
      <Typography variant={"h1"}>
        You have to be logged in to view this page.
      </Typography>
    );
  }

  return (
    <div>
      <Head>
        <title>Other page</title>
      </Head>
      <ContentPaper>
        <Typography variant={"h1"}>Hi {user.name}!</Typography>
        <Typography variant={"body1"}>
          On this page it is possible to make an API call.
        </Typography>
      </ContentPaper>
      <ContentPaper>
        <UserCan
          permission={"read:data"}
          yes={() => (
            <Button
              size="large"
              variant="contained"
              color="primary"
              onClick={(): void => setCallApi(!callApi)}
            >
              Call API
            </Button>
          )}
          no={() => (
            <Typography>
              You do not have sufficient rights to access the API.
            </Typography>
          )}
        />
        {callApi && <ApiResult />}
      </ContentPaper>
    </div>
  );
};

Other.getLayout = getLayout;

export default withAuthRequired(Other);
