import Document, { DocumentContext, DocumentInitialProps } from "next/document";
import ServerStyleSheets from "@material-ui/styles/ServerStyleSheets";
import * as React from "react";
import {
  AppPropsType,
  RenderPageResult,
} from "next/dist/next-server/lib/utils";
import { StylesProviderProps } from "@material-ui/styles";
import Processor from "postcss/lib/processor";

let prefixer: Processor;
let minifier: Processor;
if (process.env.NODE_ENV === "production") {
  /* eslint-disable global-require */
  /* eslint-disable @typescript-eslint/no-var-requires */
  const postcss = require("postcss");
  const autoprefixer = require("autoprefixer");
  const cssnano = require("cssnano");
  /* eslint-enable global-require */
  /* eslint-enable @typescript-eslint/no-var-requires */

  // Todo: imporove on these minifiers
  // You can find a benchmark of the available CSS minifiers under
  // https://github.com/GoalSmashers/css-minification-benchmark
  // We have found that clean-css is faster than cssnano but the output is larger.
  // Waiting for https://github.com/cssinjs/jss/issues/279
  // 4% slower but 12% smaller output than doing it in a single step.
  prefixer = postcss([autoprefixer]);
  minifier = postcss([cssnano]);
}

class MyDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext
  ): Promise<DocumentInitialProps> {
    const sheets = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = (): RenderPageResult | Promise<RenderPageResult> =>
      originalRenderPage({
        // useful for wrapping the whole react tree
        enhanceApp: (App) => (
          props: React.PropsWithChildren<AppPropsType>
        ): React.ReactElement<StylesProviderProps> =>
          sheets.collect(<App {...props} />),
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    let css = sheets.toString();
    if (process.env.NODE_ENV === "production") {
      // minify css
      const originalCss = sheets.toString();
      const { css: prefixedCss } = await prefixer.process(originalCss);
      const { css: minifiedCss } = await minifier.process(prefixedCss);
      css = minifiedCss;
    }

    return {
      ...initialProps,
      // Styles fragment is rendered after the app and page rendering finish.
      styles: [
        ...React.Children.toArray(initialProps.styles),
        // eslint-disable-next-line react/jsx-key
        <style
          id="jss-server-side"
          key="jss-server-side"
          //  -disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: css }}
        />,
      ],
    };
  }
}

export default MyDocument;
