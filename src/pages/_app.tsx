import "fontsource-roboto/300-normal.css";
import "fontsource-roboto/400-normal.css";
import "fontsource-roboto/500-normal.css";
import "fontsource-roboto/700-normal.css";
import { AppProps } from "next/app";
import { PageWithLayout, LayoutFunction } from "../components/layouts";
import { useEffect, useMemo, useState } from "react";
import Head from "next/head";
import {
  createMuiTheme,
  CssBaseline,
  responsiveFontSizes,
  Theme,
  ThemeProvider,
  useMediaQuery,
} from "@material-ui/core";
import { ThemeStyle, ThemeStyleContext } from "../contexts/ThemeStyleContext";
import { Auth0Provider } from "@auth0/auth0-react";
import Router from "next/router";
import * as Sentry from "@sentry/node";
import { NextPageContext } from "next";

// Initialize Sentry error reporting
Sentry.init({
  enabled: process.env.NODE_ENV === "production",
  dsn: process.env.NEXT_PUBLIC_SENTRY_DSN,
  environment: process.env.NEXT_PUBLIC_APP_ENV,
});

type AppPropsWithError = AppProps & {
  err: Pick<NextPageContext, "err">;
};

function MyApp({ Component, pageProps, err }: AppPropsWithError): JSX.Element {
  const [themeStyle, setThemeStyle] = useState<ThemeStyle>("system");

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  // Get system theme
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const theme = useMemo(
    (): Theme =>
      responsiveFontSizes(
        createMuiTheme({
          palette: {
            type:
              themeStyle === "system"
                ? prefersDarkMode
                  ? "dark"
                  : "light"
                : themeStyle,
          },
        })
      ),
    [themeStyle, prefersDarkMode]
  );

  // keep the layout between page transitions, if it is the same
  const getLayout: LayoutFunction =
    (Component as PageWithLayout).getLayout ||
    ((page: JSX.Element): JSX.Element => page);

  return (
    <>
      <Head>
        <title>My page</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
        <meta name="theme-color" content={theme.palette.primary.main} />
        {process.env.NEXT_PUBLIC_APP_ENV &&
          process.env.NEXT_PUBLIC_APP_ENV === "preview" && (
            <script
              defer
              data-project-id="18691898"
              data-project-path="hicoctopus/vangboek-next"
              data-mr-url="https://gitlab.com"
              id="review-app-toolbar-script"
              src="https://gitlab.com/assets/webpack/visual_review_toolbar.js"
            ></script>
          )}
      </Head>
      <Auth0Provider
        domain={process.env.NEXT_PUBLIC_AUTH0_DOMAIN || ""}
        clientId={process.env.NEXT_PUBLIC_AUTH0_CLIENT_ID || ""}
        redirectUri={global.window?.location.origin}
        useRefreshTokens={true}
        onRedirectCallback={(appState): void => {
          Router.replace(appState?.returnTo || "/");
        }}
      >
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <ThemeStyleContext.Provider value={{ themeStyle, setThemeStyle }}>
            {getLayout(<Component {...pageProps} err={err} />)}
          </ThemeStyleContext.Provider>
        </ThemeProvider>
      </Auth0Provider>
    </>
  );
}

export default MyApp;
