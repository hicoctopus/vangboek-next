import {
  createStyles,
  makeStyles,
  Paper,
  PaperProps,
  Theme,
} from "@material-ui/core";
import { FunctionComponent } from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    Paper: {
      padding: theme.spacing(2),
      marginTop: theme.spacing(2),
    },
  })
);

export const ContentPaper: FunctionComponent<PaperProps> = ({
  children,
  className,
  ...paperProps
}) => {
  const classes = useStyles();
  return (
    <Paper
      className={classes.Paper + (className ? " " + className : "")}
      {...paperProps}
    >
      {children}
    </Paper>
  );
};
