import { FunctionComponent } from "react";
import { ContentPaper } from "./ContentPaper";
import {
  Box,
  CircularProgress,
  createStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    centerPaper: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: "350px",
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
    pushDown: {
      marginTop: theme.spacing(6),
    },
    centerProgress: {
      display: "flex",
      justifyContent: "center",
    },
  })
);

export const LoginCheck: FunctionComponent = () => {
  const classes = useStyles();
  return (
    <ContentPaper elevation={3} component="div" className={classes.centerPaper}>
      <Typography variant="h3" component="h1" align="center">
        Checking your login
      </Typography>
      <Box
        component="div"
        className={clsx(classes.pushDown, classes.centerProgress)}
      >
        <CircularProgress
          className={classes.centerProgress}
          variant="indeterminate"
          color="primary"
        />
      </Box>
      <Typography align="center" className={classes.pushDown}>
        Please wait while we check if you are authenticated. If that is not the
        case, you will be redirected to the login page.
      </Typography>
    </ContentPaper>
  );
};
