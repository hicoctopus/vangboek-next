import { ReactElement, VoidFunctionComponent } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { JWTKeys, rolesHavePermissions } from "../lib/auth";

type UserCanProps = {
  permission: string | string[];
} & ({ yes: () => ReactElement } | { no: () => ReactElement });

export const UserCan: VoidFunctionComponent<UserCanProps> = ({
  permission,
  ...props
}) => {
  let authResult = false;
  let permissions: string[];
  if (typeof permission === "string") {
    permissions = [permission];
  } else {
    permissions = permission;
  }

  const { user } = useAuth0();
  const roles = user && user[JWTKeys.ROLES];
  if (roles) {
    let theRoles: string[];
    if (typeof roles === "string") {
      theRoles = [roles];
    } else {
      theRoles = roles;
    }
    authResult = rolesHavePermissions(theRoles, permissions);
  }

  if (authResult && "yes" in props) {
    return props.yes();
  } else if (!authResult && "no" in props) {
    return props.no();
  }
  return null;
};
