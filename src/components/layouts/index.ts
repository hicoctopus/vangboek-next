import { NextPage } from "next";

export type LayoutFunction = (page: JSX.Element) => JSX.Element;
export type PageWithLayout = NextPage & {
  getLayout?: LayoutFunction;
};
