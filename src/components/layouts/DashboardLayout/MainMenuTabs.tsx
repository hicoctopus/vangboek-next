import { FunctionComponent } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Tab, Tabs } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { MenuItemLink } from "./MainMenuItems";

type TabLinkProps = MenuItemLink & {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value?: any;
};
const TabLink: FunctionComponent<TabLinkProps> = ({
  href,
  as,
  ...tabProps
}) => (
  // Link can also handle onClick children
  <Link {...{ href, as }} passHref>
    <Tab {...tabProps} component="a" />
  </Link>
);

const useStyles = makeStyles({
  tabsFlexContainer: {
    height: "100%",
  },
});

type MainMenuProps = {
  className?: string;
  items: MenuItemLink[];
};
export const MainMenuTabs: FunctionComponent<MainMenuProps> = ({
  className,
  items,
}) => {
  const classes = useStyles();
  const { pathname } = useRouter();
  const currentItemIndex = items.findIndex((item) => item.href === pathname);
  const currentTabHref = currentItemIndex >= 0 ? pathname : false;
  return (
    <Tabs
      value={currentTabHref}
      className={className}
      classes={{
        flexContainer: classes.tabsFlexContainer,
      }}
    >
      {items.map((item) => (
        <TabLink key={item.href} value={item.href} {...item} />
      ))}
    </Tabs>
  );
};
