import { FunctionComponent, KeyboardEvent, MouseEvent } from "react";
import { MenuItemLink } from "./MainMenuItems";
import {
  createStyles,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemText,
  Theme,
} from "@material-ui/core";
import { useRouter } from "next/router";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: 250,
    },
    toolbar: theme.mixins.toolbar,
  })
);

type MainMenuDrawerProps = {
  className?: string;
  items: MenuItemLink[];
  open: boolean;
  toggleDrawer: (open: boolean) => (event: KeyboardEvent | MouseEvent) => void;
};
export const MainMenuDrawer: FunctionComponent<MainMenuDrawerProps> = ({
  className,
  items,
  open,
  toggleDrawer,
}) => {
  const classes = useStyles();
  const { pathname } = useRouter();
  const currentItemIndex = items.findIndex((item) => item.href === pathname);

  return (
    <Drawer
      anchor="left"
      variant="temporary"
      open={open}
      onClose={toggleDrawer(false)}
      className={className}
    >
      <div
        className={classes.list}
        role="presentation"
        onClick={toggleDrawer(false)}
        onKeyDown={toggleDrawer(false)}
      >
        <div className={classes.toolbar} />
        <Divider />
        <List>
          {items.map((item, index) => (
            <Link key={item.href} href={item.href} as={item.as} passHref>
              <ListItem
                button
                component="a"
                selected={index === currentItemIndex}
              >
                <ListItemText primary={item.label} />
              </ListItem>
            </Link>
          ))}
        </List>
      </div>
    </Drawer>
  );
};
