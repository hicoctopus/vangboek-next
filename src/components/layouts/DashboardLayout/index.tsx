import { FunctionComponent } from "react";
import { LayoutFunction } from "../index";
import MainAppBar from "./MainAppBar";
import {
  Container,
  makeStyles,
  createStyles,
  Theme,
  Typography,
  Box,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentBox: {
      margin: theme.spacing(3, 0),
    },
  })
);

export const DashboardLayout: FunctionComponent = ({ children }) => {
  const classes = useStyles();
  return (
    <Container maxWidth="lg">
      <MainAppBar />
      <Box className={classes.contentBox}>{children}</Box>
      <Typography
        variant="caption"
        align="center"
        color="textSecondary"
        component="div"
      >
        Some footer content
      </Typography>
    </Container>
  );
};

export const getLayout: LayoutFunction = (page) => (
  <DashboardLayout>{page}</DashboardLayout>
);
