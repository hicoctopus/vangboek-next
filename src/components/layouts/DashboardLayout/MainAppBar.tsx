import { FunctionComponent, useState } from "react";
import * as React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import {
  AppBar,
  Hidden,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import MenuIcon from "@material-ui/icons/Menu";
import { MainMenuTabs } from "./MainMenuTabs";
import { useRouter } from "next/router";
import { useAuth0 } from "@auth0/auth0-react";
import items from "./MainMenuItems";
import { MainMenuDrawer } from "./MainMenuDrawer";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginRight: theme.spacing(4),
    [theme.breakpoints.down("xs")]: {
      flexGrow: 1,
    },
  },
  menu: {
    flexGrow: 1,
    alignSelf: "stretch",
  },
}));

const MenuAppBar: FunctionComponent = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const open = Boolean(anchorEl);

  const [mainMenuOpen, setMainMenuOpen] = useState<boolean>(false);
  const toggleDrawer = (open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent
  ) => {
    if (
      event.type === "keydown" &&
      ((event as React.KeyboardEvent).key === "Tab" ||
        (event as React.KeyboardEvent).key === "Shift")
    ) {
      return;
    }

    setMainMenuOpen(open);
  };
  const { asPath: currentPath } = useRouter();
  const { isAuthenticated, isLoading, loginWithRedirect, logout } = useAuth0();

  const handleMenu = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar variant="regular">
          <Hidden smUp>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              onClick={() => setMainMenuOpen(!open)}
            >
              <MenuIcon />
            </IconButton>
            <MainMenuDrawer
              items={items}
              open={mainMenuOpen}
              toggleDrawer={toggleDrawer}
            />
          </Hidden>
          <Typography variant="h6" className={classes.title}>
            The App
          </Typography>
          <Hidden xsDown>
            <MainMenuTabs className={classes.menu} items={items} />
          </Hidden>
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <AccountCircleIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={open}
              onClose={handleClose}
            >
              {!isLoading && isAuthenticated ? (
                [
                  <MenuItem key="profile" onClick={handleClose}>
                    Profile
                  </MenuItem>,
                  <MenuItem key="account" onClick={handleClose}>
                    My account
                  </MenuItem>,
                  <MenuItem
                    key="logout"
                    onClick={(): void =>
                      logout({ returnTo: global.window?.location.origin })
                    }
                  >
                    Logout
                  </MenuItem>,
                ]
              ) : (
                <MenuItem
                  key={"login"}
                  onClick={(): Promise<void> =>
                    loginWithRedirect({ appState: { returnTo: currentPath } })
                  }
                >
                  Login
                </MenuItem>
              )}
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default MenuAppBar;
