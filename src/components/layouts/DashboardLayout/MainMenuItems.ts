export type MenuItemLink = {
  href: string;
  as?: string;
  label: string;
};
const items: MenuItemLink[] = [
  {
    href: "/",
    label: "Home",
  },
  {
    href: "/other",
    label: "Other page",
  },
];

export default items;
