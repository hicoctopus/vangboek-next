import { NextApiResponse } from "next";

/**
 * Class representing an error occurred in the API
 */
export class ApiError extends Error {
  public statusCode = 500;

  /**
   * Create an ApiError
   * @param message Error message (send to the client)
   * @param statusCode HTTP status code
   */
  public constructor(message: string, statusCode?: number) {
    super();
    this.message = message;
    this.statusCode = statusCode || 500;

    // Convert to a "real" extended error object
    this.name = this.constructor.name;
    /* istanbul ignore else*/
    if (typeof Error.captureStackTrace === "function") {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = new Error(this.getMessage()).stack;
    }
    // restore prototype chain
    Object.setPrototypeOf(this, new.target.prototype);
  }

  /**
   * @return {string}
   */
  toString(): string {
    return this.getPrettyMessage();
  }

  /**
   * @return {string}
   */
  public getPrettyMessage(): string {
    return `${this.message} - StatusCode: ${this.statusCode}.`;
  }

  /**
   * Set the message for the ApiError
   * @param message Error message (send to the client)
   */
  public setMessage = (message: string): void => {
    this.message = message;
  };

  /**
   * @return Error message of this ApiError
   */
  public getMessage = (): string => {
    return this.message;
  };

  /**
   * Set HTTP status code for this ApiError
   * @param code HTTP status code
   */
  public setStatusCode = (code: number): void => {
    this.statusCode = code;
  };

  /**
   * @return HTTP status code
   */
  public getStatusCode = (): number => {
    return this.statusCode;
  };

  /**
   * Sends the error object as JSON to the client
   * @param res
   */
  public sendToClient = (res: NextApiResponse): void => {
    res.statusCode = this.statusCode;
    res.setHeader("content-type", "application/json");
    res.end(this.createResponseJSON());
  };

  /**
   * Returns a json string representing the error object
   * @return string The json string of the error object
   */
  protected createResponseJSON = (): string => {
    const response = {
      error: true,
      message: this.message,
    };
    return JSON.stringify(response);
  };
}

/**
 * Validation Error: a 400 bad request error containing violated field names and their help message.
 */
export class ValidationError extends ApiError {
  protected fields: { [field: string]: string } = {};

  /**
   * Create an ValidationError
   * @param fieldName
   * @param message
   */
  public constructor(fieldName?: string, message?: string) {
    super("Validation error", 400);
    if (fieldName && message) {
      this.addViolatedField(fieldName, message);
    }
  }

  /**
   * Adds a violated field to the list
   * @param fieldName
   * @param violationMessage
   */
  public addViolatedField = (
    fieldName: string,
    violationMessage: string
  ): void => {
    this.fields = { ...this.fields, [fieldName]: violationMessage };
  };

  /**
   * Generates response JSON for this error
   */
  protected createResponseJSON = (): string => {
    const response = {
      error: true,
      message: this.message,
      validation: this.fields,
    };
    return JSON.stringify(response);
  };
}

// Common http responses
export class HttpBadRequest extends ApiError {
  constructor() {
    super("Bad request", 400);
  }
}

export class HttpUnauthorized extends ApiError {
  constructor() {
    super("Unauthorized", 401);
  }
}

export class HttpForbidden extends ApiError {
  constructor() {
    super("Forbidden", 403);
  }
}

export class HttpNotFound extends ApiError {
  constructor() {
    super("Not Found", 404);
  }
}

export class HttpMethodNotAllowed extends ApiError {
  constructor() {
    super("Method not allowed", 405);
  }
}

export class HttpNotAcceptable extends ApiError {
  constructor() {
    super("Not Acceptable", 406);
  }
}

export class HttpUnsupportedMedia extends ApiError {
  constructor() {
    super("Unsupported Media Type", 415);
  }
}
