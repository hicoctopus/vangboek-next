import { AsyncRequestHandler } from "./index";
import getLogger from "../logging";
import { NextApiRequest } from "next";
import {
  ApiError,
  HttpMethodNotAllowed,
  HttpNotAcceptable,
  HttpUnsupportedMedia,
} from "../ApiError";

const logger = getLogger("middleware/base");

/**
 * Checks for application/json accept header
 * @param req
 */
const checkAcceptHeader = (req: NextApiRequest): void => {
  const acceptHeader = req.headers?.accept || "";
  if ("application/json" !== acceptHeader) {
    logger.debug(
      "Accept header incorrect, expected application/json, got '%s'",
      acceptHeader
    );
    throw new HttpNotAcceptable();
  }
};

/**
 * Check if the request matches any of the required methods
 * @param requiredMethods
 * @param req
 */
const checkRequestMethod = (
  requiredMethods: string | string[],
  req: NextApiRequest
): void => {
  if (!req.method) {
    logger.warn("No value set for `req.method`");
    throw new HttpMethodNotAllowed();
  }
  const requestMethod = req.method.toUpperCase();
  // Star means all methods are allowed
  if (requiredMethods !== "*") {
    requiredMethods = Array.isArray(requiredMethods)
      ? requiredMethods
      : [requiredMethods];
    if (!requiredMethods.includes(requestMethod)) {
      logger.debug(
        "Expected method(s) %o but got %s.",
        requiredMethods,
        requestMethod
      );
      throw new HttpMethodNotAllowed();
    }
  }
};

/**
 * Checks for json content
 * @param req
 */
const checkRequestContentType = (req: NextApiRequest): void => {
  const contentTypeHeader = req.headers["content-type"] || "";
  if ("application/json" !== contentTypeHeader) {
    logger.debug("Expected json body, but got type '%s'", contentTypeHeader);
    throw new HttpUnsupportedMedia();
  }
};

/**
 * Middleware function required for all API handlers that does some basic checks and catches all the errors.
 * @param allowedMethods
 * @param requestHandler
 */
export const baseMiddleware = (
  allowedMethods: string | string[] = "*",
  requestHandler: AsyncRequestHandler
): AsyncRequestHandler => {
  return async (req, res): Promise<void> => {
    const startTime = new Date().getTime();
    try {
      checkAcceptHeader(req);
      checkRequestMethod(allowedMethods, req);
      if (
        ["POST", "PUT", "PATCH"].includes(req.method?.toUpperCase() || "") &&
        req.body
      ) {
        checkRequestContentType(req); // expect json body
      } else {
        // other methods should not have a body.
        req.body = undefined;
      }

      await requestHandler(req, res);

      // Finalize the request if needed.
      if (!res.finished) {
        logger.debug(
          "Need to call response.end from the base middleware. Are you forgotten to call it in the request handler yourself?"
        );
        res.end();
      }
    } catch (err) {
      if (!(err instanceof ApiError)) {
        // no api error, return 500
        logger.error(
          {
            error: { message: err.message, stack: err.stack },
          },
          "Non API error caught during request handling."
        );
        const apiError = new ApiError("Internal server error", 500);
        apiError.sendToClient(res);
      } else {
        err.sendToClient(res);
      }
    }
    const requestDuration = new Date().getTime() - startTime;
    logger.debug(
      "%s %s resulted in a %d. Took %d ms",
      req.method?.toUpperCase(),
      req.url,
      res.statusCode,
      requestDuration
    );
  };
};
