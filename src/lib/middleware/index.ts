import { NextApiRequest, NextApiResponse } from "next";
import { checkAuthMiddleware, JwtPayload } from "./auth";
import { baseMiddleware } from "./base";

export type AuthorizedApiRequest = NextApiRequest & { token: JwtPayload };

export type AsyncRequestHandler<T = NextApiRequest> = (
  req: T,
  res: NextApiResponse
) => Promise<void>;

export default baseMiddleware;
export { baseMiddleware as base, checkAuthMiddleware as checkAuth };
