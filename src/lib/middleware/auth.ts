import { IncomingMessage } from "http";
import { GetPublicKeyOrSecret, verify } from "jsonwebtoken";
import jwksRsa from "jwks-rsa";
import { NextApiRequest, NextApiResponse } from "next";
import { HttpBadRequest, HttpForbidden, HttpUnauthorized } from "../ApiError";
import getLogger from "../logging";
import { AuthorizedApiRequest, AsyncRequestHandler } from "./index";

const logger = getLogger("middleware/auth");

export interface JwtPayload {
  iss: string;
  sub: string;
  aud: string[];
  iat: number;
  exp: number;
  azp: string;
  scope: string;
  permissions: string[];
}

/**
 * @var jwksClient
 * The client that fetches the keys to validate tokens
 */
let jwksClient: jwksRsa.JwksClient | null = null;
/**
 * Returns JWKS client.
 * Initializes a new one when it doesn't exist
 */
const getJwksClient = (): jwksRsa.JwksClient => {
  if (null === jwksClient) {
    jwksClient = jwksRsa({
      strictSsl: true,
      jwksUri: `https://${process.env.NEXT_PUBLIC_AUTH0_DOMAIN}/.well-known/jwks.json`,
      cache: true,
      rateLimit: true,
    });
  }
  return jwksClient;
};

/**
 * Returns the key for the given keyID in the JWT header.
 * @param header
 * @param callback
 */
const getKeyFromJwtHeader: GetPublicKeyOrSecret = (header, callback) => {
  if (!header.kid) {
    logger.info("No KeyID in token header.");
    throw new HttpBadRequest();
  }
  getJwksClient().getSigningKey(header.kid, (err, key) => {
    if (err) {
      logger.warn({ error: err }, "Unable to retrieve signing key.");
      callback(err);
    } else {
      callback(null, key.getPublicKey());
    }
  });
};
/**
 * Fetches authorization token from the request. Returns null when there is no token.
 * @param req
 */
const extractTokenFromRequest = (req: IncomingMessage): string | null => {
  let token = null;
  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(" ");
    if (parts.length === 2 && /^Bearer$/.test(parts[0])) {
      token = parts[1];
    }
  }
  return token;
};

/**
 * Verifies a JWT token (inclusive audience and issuer).
 * @param token
 */
const verifyToken = async (token: string): Promise<JwtPayload> => {
  return new Promise<JwtPayload>((resolve, reject) => {
    verify(
      token,
      getKeyFromJwtHeader,
      {
        audience: process.env.NEXT_PUBLIC_API_IDENTIFIER,
        issuer: `https://${process.env.NEXT_PUBLIC_AUTH0_DOMAIN}/`,
        algorithms: ["RS256"],
      },
      (err, decoded) => {
        if (err) {
          reject(err);
        } else {
          resolve(decoded as JwtPayload);
        }
      }
    );
  });
};

/**
 * Check if the current logged in user has the required permissions.
 * Throws a HTTP forbidden error when the user does not suffice.
 *
 * @param req
 * @param requiredPermissions
 */
export const requirePermissions = (
  req: AuthorizedApiRequest,
  requiredPermissions: string[]
): void => {
  if (
    typeof req.token.permissions == "undefined" ||
    !requiredPermissions.every((permission) =>
      req.token.permissions.includes(permission)
    )
  ) {
    logger.info(
      "User has not the required permissions. Required: %o Available: %o",
      requiredPermissions,
      req.token.permissions || "no token or permissions available"
    );
    throw new HttpForbidden();
  }
};

/**
 * Middleware that checks if the current request contains a valid token.
 * If that is the case, the token is accessible by req.token
 *
 * @param requiredPermissions
 * @param requestHandler
 */
export const checkAuthMiddleware = (
  requiredPermissions: string[],
  requestHandler: AsyncRequestHandler<AuthorizedApiRequest>
): AsyncRequestHandler => {
  return async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
    const token = extractTokenFromRequest(req);
    if (null == token) {
      logger.info("Token required, but none given.");
      throw new HttpBadRequest();
    }
    let decoded: JwtPayload;
    try {
      decoded = await verifyToken(token);
    } catch (e) {
      logger.info({ error: e }, "Invalid JWT.");
      throw new HttpUnauthorized();
    }

    // add token to the request
    const enrichedRequest = req as AuthorizedApiRequest;
    enrichedRequest.token = decoded;

    requirePermissions(enrichedRequest, requiredPermissions);

    await requestHandler(enrichedRequest, res);
  };
};
