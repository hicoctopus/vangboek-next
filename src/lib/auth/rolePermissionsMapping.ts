/* In the future, dynamic roles may be added, like this:
    {
        // ...
        Role: {
            dynamic: {
                "edit:something": ({some, data}) => return some === data
            }
       // ...
    }

    This can be used for example to allow the author to edit his own posts, while an admin has always the same permission.
 */
type RolePermissions = {
  static: string[];
};
type RolePermissionsMapping = {
  [key: string]: RolePermissions;
};

/* istanbul ignore */
const rolePermissionsMapping: RolePermissionsMapping = {
  Datareader: {
    static: ["read:data"],
  },
};

export default rolePermissionsMapping;
