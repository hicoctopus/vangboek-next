import roles from "./rolePermissionsMapping";

/**
 * Checks if the given role has the requested permission.
 * @param role
 * @param permission
 */
export const roleHasPermission = (
  role: string,
  permission: string
): boolean => {
  const allowedPermissions = roles[role];
  if (!allowedPermissions) {
    // Role not found
    return false;
  }

  // Check static permissions
  return !!allowedPermissions.static?.includes(permission);
};

/**
 * Checks if the given role has all the requested permissions
 * @param role
 * @param permissions
 */
export const roleHasPermissions = (
  role: string,
  permissions: string[]
): boolean => {
  return permissions.reduce(
    (previous: boolean, permission: string) =>
      previous && roleHasPermission(role, permission),
    true
  );
};

/**
 * Checks all the permissions are available in the given roles
 * @param roles
 * @param permissions
 */
export const rolesHavePermissions = (
  roles: string[],
  permissions: string[]
): boolean => {
  return permissions.reduce(
    (previous: boolean, permission: string) =>
      previous &&
      roles.map((role) => roleHasPermission(role, permission)).includes(true),
    true
  );
};
