export * from "./rolePermissionsMapping";
export * from "./permissionCheck";

/**
 * Available JWT Keys.
 * Custom ones are prefixed with a namespace - see auth0 specs.
 */
export enum JWTKeys {
  ROLES = "https://vangboek-next/roles",
}
