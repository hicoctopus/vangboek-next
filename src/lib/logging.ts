import pino from "pino";

const mainLogger = pino({
  prettyPrint:
    process.env.NODE_ENV === "development"
      ? {
          translateTime: true,
        }
      : false,
  level: process.env.NODE_ENV === "production" ? "info" : "debug",
  enabled: process.env.NODE_ENV !== "test",
});

/**
 * Returns a logger which keeps track of the current module
 * @param moduleName
 */
const getLogger = (moduleName: string): pino.Logger => {
  return mainLogger.child({
    module: moduleName,
  });
};

// Ignore process error handlers within coverage
/* istanbul ignore if */
if (process.env.NODE_ENV !== "test") {
  // Log when the process exits unexpectedly
  process.on(
    "uncaughtException",
    pino.final(mainLogger, (err, finalLogger) => {
      finalLogger.error(err, "uncaughtException");
      process.exit(1);
    })
  );

  process.on(
    "unhandledRejection",
    pino.final(mainLogger, (err, finalLogger) => {
      finalLogger.error(err, "unhandledRejection");
      process.exit(1);
    }) as NodeJS.UnhandledRejectionListener
  );
}

export default getLogger;
export { getLogger, mainLogger };
