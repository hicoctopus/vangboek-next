module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:sonarjs/recommended",
  ],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["react", "react-hooks", "@typescript-eslint", "sonarjs"],
  settings: {
    react: {
      version: "detect",
    },
  },
  overrides: [
    {
      // typescript rules for typescript files
      files: ["**/*.{ts,tsx}"],
      extends: [
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "prettier/@typescript-eslint",
      ],
    },
    {
      // test files
      files: ["**/__tests__/**"],
      env: { jest: true },
    },
    {
      // node environment on api routes
      files: ["src/pages/api"],
      env: { node: true },
    },
    {
      // react (component) files
      files: ["**/*.{tsx,jsx}"],
      extends: ["plugin:react/recommended", "plugin:react-hooks/recommended"],
      rules: {
        "react/prop-types": 0, // no prop-types are used in this project
        // since using the new JSX transformer (Next.js 9.5.3+)
        "react/jsx-uses-react": "off",
        "react/react-in-jsx-scope": "off",
      },
    },
    {
      files: ["src/pages/**/*.tsx"],
      rules: {
        "react/react-in-jsx-scope": 0, // Next.js injects react in all those pages.
      },
    },
    {
      // Test lint rules
      files: ["__tests__/**/*.{ts,tsx,js,jsx}"],
      plugins: ["jest"],
      env: {
        "jest/globals": true,
      },
      extends: ["plugin:jest/recommended", "plugin:jest/style"],
      rules: {
        "jest/consistent-test-it": 1,
        "jest/lowercase-name": 1,
        "jest/no-expect-resolves": 2,
        "jest/no-large-snapshots": 2,
        "jest/prefer-todo": 2,
        "jest/valid-title": 2,
        "sonarjs/no-duplicate-string": "off",
      },
    },
  ],
  rules: {
    "react/prop-types": 0, // no prop-types are used in this project
  },
};
